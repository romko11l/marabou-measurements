import tensorflow as tf
from maraboupy import Marabou

mnist = tf.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

N = 24

network = Marabou.read_tf(filename="./res-" + str(N), modelType="savedModel_v2")

inputVars = network.inputVars[0][0]
outputVars = network.outputVars[0][0]

eps = 0.1

# x_train[1] is image with 0
for i in range(0, 28):
    for j in range(0, 28):
        lowerBound = max(x_train[1][i][j] - eps, 0.0)
        network.setLowerBound(inputVars[i][j], lowerBound)

        upperBound = min(x_train[1][i][j] + eps, 1.0)
        network.setUpperBound(inputVars[i][j], upperBound)

# y_0 - y_1 <= 0
network.addInequality([outputVars[0], outputVars[1]], [1, -1], 0, isProperty=True)

exitCode, vals, stats = network.solve("marabou.log")

print("===========================")

print(exitCode)
print(vals)
print(stats.getTotalTimeInMicro())
